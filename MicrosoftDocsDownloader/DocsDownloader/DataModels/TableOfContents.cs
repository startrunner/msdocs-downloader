﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocsDownloader.DataModels
{
    public class TableOfContents
    {
        public IReadOnlyList<ContentItem> Items { get; private set; }

        public TableOfContents(IReadOnlyList<ContentItem> items)
        {
            Items = items;
        }

        internal int GetTotalItems()
        {
            return Items.Count + Items.Select(x => x.GetNumberOfDesdendants()).Sum();
        }
    }
}
