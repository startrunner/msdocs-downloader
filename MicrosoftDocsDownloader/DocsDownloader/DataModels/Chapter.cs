﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocsDownloader.DataModels
{
    public class Chapter
    {
        public string Title { get; }
        public string HtmlContent { get; }

        public Chapter(string title, string htmlContent)
        {
            Title = title;
            HtmlContent = htmlContent;
        }


    }
}
