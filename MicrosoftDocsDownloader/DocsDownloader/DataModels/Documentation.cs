﻿namespace DocsDownloader.DataModels
{
    public class Documentation
    {
        private TableOfContents TableOfContents { get; }
        private string Filename { get; }

        public Documentation(TableOfContents tableOfContents, string filename)
        {
            this.TableOfContents = tableOfContents;
            this.Filename = filename;
        }
    }
}