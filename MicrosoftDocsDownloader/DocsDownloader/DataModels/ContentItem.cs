﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System;

namespace DocsDownloader.DataModels
{
    public class ContentItem
    {
        [JsonProperty("children")]
        private ContentItem[] children = new ContentItem[0];

        [JsonProperty("toc_title")]
        public string Title { get; private set; }

        [JsonProperty("href")]
        public string DocumentName { get; private set; }

        public IReadOnlyList<ContentItem> Children { get => children; }

        internal int GetNumberOfDesdendants()
        {
            return children.Length + children.Select(x => x.GetNumberOfDesdendants()).Sum();
        }

        public bool HasChildren { get => children?.Length != 0; }
    }
}
