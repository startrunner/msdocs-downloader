﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocsDownloader.Gathering
{
    static class UrlWorks
    {
        static readonly char[] UrlSeparators =new char[]{ '/'};
        public static string ReplaceLastSegment(string url, string segment)
        {
            url = url.Trim().Trim(UrlSeparators);

            string[] urlSplit = url.Split(UrlSeparators, StringSplitOptions.None);
            urlSplit[urlSplit.Length - 1] = segment;
            return string.Join("/", urlSplit);
        }

        public static string RemoveArgs(string url)
        {
            return url.Split('?').First();
        }

        public static string GetExtension(string url)
        {
            url = url.Trim().Trim(UrlSeparators);
            return RemoveArgs(url).Split('.').LastOrDefault();
        }
    }
}
