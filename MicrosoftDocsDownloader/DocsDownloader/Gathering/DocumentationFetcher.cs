﻿using DocsDownloader.DataModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocsDownloader.Gathering
{
    public class DocumentationFetcher
    {
        private string pageUrl;

        public DocumentationFetcher(string pageUrl)
        {
            this.pageUrl = pageUrl;
        }

        void RecursiveFetch(TableOfContents contents, string outputFilename)
        {
            StreamWriter buildTo = new StreamWriter(outputFilename);
            RecursiveInfo info = new RecursiveInfo()
            {
                buildTo = buildTo,
                current = 1,
                total = contents.GetTotalItems(),
                level = 1,
                filename = outputFilename,
                downloader = new FileDownloader($"{outputFilename}_files")
            };

            buildTo.WriteLine("<!doctype html>");
            buildTo.WriteLine("<html>");
            buildTo.WriteLine("<head>");
            buildTo.WriteLine(@"<style>li p {display:inline;}/*kindle fucks this up and shows bullets and their text on separate line*/</style>");
            buildTo.WriteLine("</head>");
            buildTo.WriteLine("<body>");

            foreach(var contentItem in contents.Items)
            {
                RecursiveFetch(contentItem, info);
                info.current++;
            }

            buildTo.WriteLine("</body></html>");
            buildTo.Close();
        }

        class RecursiveInfo
        {
            public int current;
            public int total;
            public StreamWriter buildTo;
            public int level;
            public string filename;
            public FileDownloader downloader = null;
        }

        private void RecursiveFetch(ContentItem contentItem, RecursiveInfo info)
        {

            string message = $"{new string(' ', (info.level - 1) * 2)} {contentItem.Title} ({info.current} of {info.total})";

            Debug.WriteLine(message);
            Console.WriteLine(message);

            string documentName = contentItem.DocumentName;
            string chapterAddress = UrlWorks.ReplaceLastSegment(pageUrl, documentName);
            var chapter = new ChapterFetcher(chapterAddress, info.downloader).Fetch();
            info.buildTo.WriteLine($"<h1 msdd>{new string('>', (info.level - 1))}{chapter.Title}</h1>");
            info.buildTo.WriteLine(chapter.HtmlContent);

            if(contentItem.HasChildren)
            {
                foreach(var child in contentItem.Children)
                {
                    info.level++;
                    info.current++;
                    RecursiveFetch(child, info);
                    info.level--;
                }
            }
        }

        public Documentation Fetch(string filename=null)
        {
            filename = filename ?? Path.GetTempFileName();
            var tableOfContents = new TableOfContentsFetcher(pageUrl).Fetch();
            RecursiveFetch(tableOfContents, filename);

            return new Documentation(tableOfContents, filename);
        }
    }
}
