﻿using DocsDownloader.DataModels;
using DocsDownloader.Web;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DocsDownloader.Gathering
{
    public class ChapterFetcher
    {
        private string pageUrl;
        private FileDownloader downloader;

        public ChapterFetcher(string pageUrl, FileDownloader downloader = null) 
        {
            if(downloader==null)
            {
                downloader = new FileDownloader(Path.GetTempPath());
            }
            this.pageUrl = pageUrl;
            this.downloader = downloader;
        }

        public Chapter Fetch()
        {
            var htmlDocument = GetDocument();

            var divMain = htmlDocument.GetElementbyId("main");
            var h1Title = divMain.Descendants()
                .First(x => x.Name == "h1" /*&& x.Attributes.Contains("sourcefile")*/);
            string chapterTitle = h1Title.InnerText;

            FixImageTags(htmlDocument);
            FixHrefs(htmlDocument);

            var contentDiv = divMain
                .Descendants()
                .Where(x => x.Name == "div" && x.HasAttributes)
                .First(x => (x.Attributes["class"]?.Value?.Contains("content")) ?? false);

            ;

            return new Chapter(chapterTitle, contentDiv.InnerHtml);
        }

        private void FixImageTags(HtmlDocument document)
        {
            var imageTags = document
                .DocumentNode
                .Descendants()
                .Where(x => x.Name == "img");

            foreach(var tag in imageTags)
            {
                var srcAttribute = tag.Attributes["src"];
                string originalUrl = srcAttribute.Value;
                string absoluteUrl = null;

                if (!originalUrl.Trim().Normalize().StartsWith("http".Normalize()))
                {
                    absoluteUrl = UrlWorks.ReplaceLastSegment(pageUrl, srcAttribute.Value);
                }
                else
                {
                    absoluteUrl = originalUrl;
                }

                string localPath = downloader.DownloadFile(absoluteUrl);
                srcAttribute.Value = localPath;
            }
        }

        private void FixHrefs(HtmlDocument htmlDocument)
        {
            var imageTags = htmlDocument
                .DocumentNode
                .Descendants()
                .Where(x => x.Name == "a");

            foreach (var tag in imageTags)
            {
                var srcAttribute = tag.Attributes["href"];
                string oldUrl = srcAttribute?.Value;
                if (oldUrl != null && !oldUrl.Trim().Normalize().StartsWith("http".Normalize()))
                {
                    string newUrl = UrlWorks.ReplaceLastSegment(pageUrl, srcAttribute.Value);
                    srcAttribute.Value = newUrl;
                }
            }
        }

        HtmlDocument GetDocument()
        {
            HtmlDownloader loader = new HtmlDownloader(pageUrl);
            return loader.Download();
        }
    }
}
