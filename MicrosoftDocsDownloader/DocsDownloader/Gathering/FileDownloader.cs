﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DocsDownloader.Gathering
{
    public class FileDownloader
    {
        private string directoryPath;
        Dictionary<string, string> urlFileAssoc = new Dictionary<string, string>();

        public FileDownloader(string directoryPath)
        {
            this.directoryPath = directoryPath;
        }

        public string DownloadFile(string fileUrl)
        {
            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }

            fileUrl = UrlWorks.RemoveArgs(fileUrl);
            string extension = UrlWorks.GetExtension(fileUrl);
            string filePath = Path.Combine(directoryPath, $"{Guid.NewGuid():N}.{extension}");

            if(urlFileAssoc.ContainsKey(fileUrl))
            {
                return urlFileAssoc[fileUrl];
            }

            try
            {
                WebClient client = new WebClient();
                client.DownloadFile(Uri.EscapeUriString(fileUrl), filePath);
                urlFileAssoc[fileUrl] = filePath;
            }
            catch (WebException)
            {

            }
            return filePath;
        }
    }
}
