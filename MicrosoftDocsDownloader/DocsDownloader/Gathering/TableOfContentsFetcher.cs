﻿using DocsDownloader.DataModels;
using DocsDownloader.Web;
using HtmlAgilityPack;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace DocsDownloader.Gathering
{
    public class TableOfContentsFetcher
    {
        private string pageUrl;

        public TableOfContentsFetcher(string pageUrl)
        {
            this.pageUrl = pageUrl;
        }

        HtmlDocument FetchDocument()
        {
            HtmlDownloader loader = new HtmlDownloader(pageUrl);
            return loader.Download();
        }

        public TableOfContents Fetch()
        {
            HtmlDocument document=FetchDocument();
            var node = document.DocumentNode;

            var metaTableOfContents = node
                .Descendants()
                .First(x => x.Name == "meta" && x.Attributes["name"]?.Value == "toc_rel");

            string tableOfContentsFileName = metaTableOfContents.Attributes["content"].Value;
            string tableOfContentsFileUrl = UrlWorks.ReplaceLastSegment(pageUrl, tableOfContentsFileName);
            IReadOnlyList<ContentItem> contentItems = FetchContents(tableOfContentsFileUrl);
            return new TableOfContents(contentItems);
        }

        IReadOnlyList<ContentItem> FetchContents(string jsonUrl)
        {
            string json = FetchContentsJson(jsonUrl);
            ContentItem[] items = JsonConvert.DeserializeObject<ContentItem[]>(json);
            return items;
        }

        string FetchContentsJson(string jsonUrl)
        {
            HttpWebRequest request = WebRequest.CreateHttp(jsonUrl);
            request.Accept = "text/json";
            request.UserAgent = Constants.UserAgent;

            WebResponse response = request.GetResponse();
            string json;

            using (var responseStream = response.GetResponseStream())
            {
                StreamReader reader = new StreamReader(responseStream);
                json = reader.ReadToEnd();
                reader.Close();
            }

            return json;
        }
    }
}
