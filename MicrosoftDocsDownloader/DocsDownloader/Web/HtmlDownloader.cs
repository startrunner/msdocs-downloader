﻿using HtmlAgilityPack;
using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Windows;
using System.Threading;

namespace DocsDownloader.Web
{
    public  class HtmlDownloader
    {
        private string pageUrl;

        public HtmlDownloader(string pageUrl)
        {
            this.pageUrl = pageUrl;
        }

        public HtmlDocument Download()
        {
            var request = WebRequest.CreateHttp(new Uri(pageUrl));
            request.UserAgent = Constants.UserAgent;

            var response = request.GetResponse();
            string responseText;

            using (var responseStream = response.GetResponseStream())
            {
                var reader = new StreamReader(responseStream);
                responseText = reader.ReadToEnd();
                reader.Close();
            }
            response.Close();

            ;


            HtmlDocument document = new HtmlDocument();
            document.LoadHtml(responseText);
            ;
            return document;
        }
    }
}
