﻿using DocsDownloader.Gathering;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DocsDownloader
{
    class DocsDownloader
    {
        public static void Main()
        {
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

            Console.WriteLine("Microsoft Docs Downloader by Alexander Ivanov");
            Console.WriteLine("https://bitbucket.org/startrunner/msdocs-downloader");

            string url;
            string filename = "";

            Console.Write("Url: ");
            url = Console.ReadLine().Trim();

            Console.Write("Filename: ");

            for (bool valid = false; !valid;)
            {
                filename = Console.ReadLine().Trim();
                try
                {
                    if (File.Exists(filename)) File.Delete(filename);
                    valid = true;
                }
                catch
                {
                    Console.WriteLine("File already exists and could not be overwritten. Please try another one."); ;
                    continue;
                }
            }

            Console.WriteLine($"{url}  --->  {filename}");

            DocumentationFetcher fetcher = new DocumentationFetcher(url);
            var doc = fetcher.Fetch(filename);

            Console.WriteLine("Done!");
        }
    }
}
