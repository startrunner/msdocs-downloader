﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DocsDownloader.Web;
using DocsDownloader.Gathering;

namespace DocsDownloader.Testing
{
    [TestClass]
    public class HtmlDownloaderTests
    {
        static readonly string Address= @"https://docs.microsoft.com/en-us/visualstudio/extensibility/extensibility-in-visual-studio";
        [TestMethod]
        public void DebugHtmlDownloader()
        {
            var result=new HtmlDownloader(Address).Download();
            ;
        }

        [TestMethod]
        public void DebugContentListFetcher()
        {
            new TableOfContentsFetcher(Address).Fetch();
        }

        [TestMethod]
        public void DebugChapterFetcher()
        {
            new ChapterFetcher(Address).Fetch();
        }

        [TestMethod]
        public void DebugDocumentationFetcher()
        {
            new DocumentationFetcher(Address).Fetch();
        }
    }
}
